#!/usr/bin/python3

# For fetchig website
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
# For waiting data on website
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# Action chains
from selenium.webdriver.common.action_chains import ActionChains
# For secure password
from getpass import getpass

login = input("Login: ")
password = getpass()

options = Options()

# Comment out if want to see process
options.headless = True

# Requires https://github.com/mozilla/geckodriver/releases
browser = webdriver.Firefox(options=options)
browser.get('https://my.mosenergosbyt.ru/auth')

cats = [
    'ВЗНОС НА КАП. РЕМОНТ',
    'ВОДООТВЕДЕНИЕ',
    'ГОРЯЧЕЕ В/С',
    'ГОРЯЧЕЕ В/С ОДН (КВ.М)',
    'ДОБРОВОЛЬНОЕ СТРАХОВАНИЕ',
    'ЗАПИРАЮЩЕЕ УСТРОЙСТВО',
    'ОБРАЩЕНИЕ С ТКО',
    'СОДЕРЖАНИЕ И РЕМОНТ ЖИЛОГО ПОМЕЩЕНИЯ',
    'ТЕЛЕВИДЕНИЕ (КАБЕЛЬНОЕ ИЛИ АНТЕННА)',
    'ХОЛОДНОЕ В/С',
    'ХОЛОДНОЕ В/С ОДН (КВ.М)',
    'ЭЛЕКТРОСНАБЖЕНИЕ',
    'ОТОПЛЕНИЕ КПУ',
]

try:
    try:
        # Wait for stupid covering message to appear and skip it
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.XPATH, '//button[@testid="skip"]'))
        )
        skip_button = browser.find_element_by_xpath('//button[@testid="skip"]')
        skip_button.click()
    except:
        pass

    # Wait 10 sec for page load
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.NAME, 'login'))
    )

    login_box = browser.find_element_by_name('login')
    login_box.send_keys(login)

    password_box = browser.find_element_by_name('password')
    password_box.send_keys(password + Keys.RETURN)

    try:
        # Wait for stupid covering message to appear and skip it
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.XPATH, '//button[@testid="skip"]'))
        )
        skip_button = browser.find_element_by_xpath('//button[@testid="skip"]')
        skip_button.click()
    except:
        pass

    try:
        # Wait for stupid covering covid-19 message to appear and skip it
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.XPATH, '//div[@testid="formContainer"]'))
        )
        form_container = browser.find_element_by_xpath('//div[@testid="formContainer"]')
        act = ActionChains(browser);
        act.move_to_element(form_container).move_by_offset(320, 0).click().perform()
    except:
        pass

    # Switch to EPD
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, '//a[@href="/accounts/4213062"]'))
    )
    account = browser.find_element_by_xpath('//a[@href="/accounts/4213062"]')
    account.click()

    # Wait 10 sec for page reload
    WebDriverWait(browser, 1000).until(
        EC.presence_of_element_located((By.XPATH, '//div[@testid="PaymentDoc-Panel-0-eventItem"]'))
    )

    #/html/body/div/div/div[2]/div[2]/div[2]/div/div[1]/div[3]/div/div[1]

    payment_doc = browser.find_element_by_xpath('//div[@testid="PaymentDoc-Panel-0-eventItem"]')
    payment_doc.click()

    #/html/body/div/div/div[2]/div[2]/div[2]/div/div[1]/div[3]/div/div[1]/div[2]/div/div[1]/div/div/div/div[2]/button

    details = payment_doc.find_element_by_xpath('.//div[2]/button')
    details.click()

    results={}
    # In winter we also have a bill for central hitting here
    for i in range(20):
        try:
            category = payment_doc.find_element_by_xpath('.//div[@testid="-Panel-' + str(i) + '-eventItem"]')
        except:
            break
        category.click()

        header = category.find_element_by_xpath('.//div[@testid="-header-' + str(i) + '"]').text
        charged = category.find_element_by_xpath('.//div[@testid="BRowField-sm_charged"]').text

        header = header.splitlines()[0].strip()
        charged = charged.splitlines()[1].strip()
        charged = charged.replace(' ', '')
        charged = charged.replace(',', '.')
        charged = charged.replace('руб.', '')
        if header in results or header not in cats:
            print("Warn: Lost %s; %s" % (header, results[header]))
        results[header] = charged

    for cat in cats:
        if cat in results:
            print("%s; %s" % (cat, results[cat]))
        else:
            print("%s; %s" % (cat, '0.0'))

finally:
    browser.quit()
